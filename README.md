# Ventura

A fountain ("plain text markup language for screenwriting") file processor.

## Description

This tool converts `.fountain` files (a markup language for cinema screenwriting) to the `.rtf` format. You can find all information and documents about this format in [fountain.io](https://fountain.io/).

## Building and installation

Run `make` and then `make install` as root.

## Usage

```sh
ventura [-f {output format}] {input file} [{output file}]
```

The input file must be a `.fountain` file. The output file extension can be `.rtf` for RTF files or `.json`, which is not very useful.

File names can be - (hyphen) to mean stdin or stdout. If the output file is not defined, it's dumped to stdout.

You can use the `-f` optional flag to set the output format (`rtf` or `json`), in case it's dumped to stdout.

## Trivia

Ventura borrows its name from [Ventura Rodríguez](https://en.wikipedia.org/wiki/Ventura_Rodr%C3%ADguez), a great spanish architect and artist, creator of some of the most beautiful *fountains* in Madrid.

If you are old enough, you may remember an ancient desktop publishing software with a similar name; obviously, it has no relation with this tool.

## License

See the LICENSE file for details.

## Author

grunfink [@grunfink@comam.es](https://comam.es/snac/grunfink)

Buy grunfink a coffee: https://ko-fi.com/grunfink
