/* ventura - a .fountain processor */
/* copyright (c) 2023 - 2024 grunfink - MIT license */

#include "xs.h"
#include "xs_io.h"
#include "xs_unicode.h"
#include "xs_json.h"
#include "xs_match.h"
#include "xs_regex.h"

#include "ventura.h"


static int is_scene(const char *line)
/* returns true if it's a scene heading */
{
    /* special case: lines starting with just one . */
    if (line[0] == '.' && (line[1] && line[1] != '.'))
        return 1;

    return xs_match(line, "INT*|EXT*|EST*|INT./EXT*|INT/EXT*|I/E*");
}


static int is_character(const char *line)
/* returns true if it's a character name */
{
    int n = 0;
    const char *p = line;
    unsigned int cpoint;

    if (*line == '\0' || *line == ' ')
        return 0;

    /* special case: lines starting with @ */
    if (*line == '@')
        return 1;

    while ((cpoint = xs_utf8_dec(&p))) {
        /* if already started scanning and a ( or ^ is found, it's valid */
        if (n && (cpoint == '(' || cpoint == '^'))
            break;

        if (cpoint != ' ' && cpoint != '\'' && cpoint != '.' && !xs_unicode_is_upper(cpoint))
            return 0;

        n++;
    }

    return 1;
}


static int is_transition(const char *line)
/* returns true if it's a transition */
{
    if (xs_match(line, "* TO:|* A:")) {
        int m, c = strlen(line);
        const char *p = line;

        /* it must also be made of all caps */
        for (m = 0; m < c - 2; m++) {
            unsigned int cpoint = xs_utf8_dec(&p);

            if (cpoint != ' ' && !xs_unicode_is_upper(cpoint))
                return 0;
        }

        return 1;
    }

    return 0;
}


static xs_dict *new_unit(const char *type)
{
    xs_dict *d = xs_dict_new();
    xs_list *c = xs_list_new();

    d = xs_dict_append(d, "type",    type);
    d = xs_dict_append(d, "content", c);

    return d;
}


xs_list *fountain_parse(FILE *f)
/* parses the lines of a .fountain file */
{
    xs_list *r = xs_list_new();
    enum { M_HEADER, M_SCENE, M_ACTION, M_DIALOG, M_TRANSITION, M_CENTER } mode = M_HEADER;
    const char *m_types[] = { "header", "scene", "action", "dialog", "transition", "center" };
    xs *scenes = xs_list_new();
    xs *characters = xs_list_new();

    xs_dict *d = new_unit(m_types[mode]);

    while (!feof(f)) {
        xs *ln = xs_readline(f);

        if (ln == NULL)
            break;

        ln = xs_strip_chars_i(ln, "\r\n");

again:
        if (mode == M_HEADER) {
            if (*ln == '\0') { /** HEADER **/
                mode = M_ACTION;
                goto switch_mode;
            }

            xs *c = xs_dup(xs_dict_get(d, "content"));

            if (ln[0] == ' ' || ln[0] == '\t') {
                /* part of previous header */
                ln = xs_strip_i(ln);
                c = xs_list_append(c, ln);
            }
            else
            if (xs_str_in(ln, ": ") != -1) {
                /* split */
                xs *l1 = xs_split_n(ln, ": ", 1);
                xs *s1 = xs_fmt("#%s", xs_list_get(l1, 0));
                c = xs_list_append(c, s1);
                if (xs_list_len(l1) == 2)
                    c = xs_list_append(c, xs_list_get(l1, 1));
            }
            else
            if (xs_str_in(ln, ":") != -1) {
                /* it's a header by itself */
                ln = xs_strip_chars_i(ln, " \t:");
                ln = xs_str_prepend_i(ln, "#");
                c = xs_list_append(c, ln);
            }

            d = xs_dict_set(d, "content", c);

            continue;
        }
        else
        if (mode == M_ACTION) {
            if (is_scene(ln))   /** ACTION **/
                mode = M_SCENE;
            else
            if (is_character(ln))
                mode = M_DIALOG;
            else
            if (is_transition(ln))
                mode = M_TRANSITION;
            else
            if (xs_match(ln, ">*<"))
                mode = M_CENTER;

            if (mode != M_ACTION) {
                /* mode changed? crop empty leading lines */
                xs *c = xs_dup(xs_dict_get(d, "content"));
                const char *str;

                while ((str = xs_list_get(c, -1)) && *str == '\0')
                    c = xs_list_del(c, -1);

                d = xs_dict_set(d, "content", c);

                goto switch_mode;
            }

            xs *c = xs_dup(xs_dict_get(d, "content"));
            if (*ln != '\0' || xs_list_len(c)) {
                c = xs_list_append(c, ln);
                d = xs_dict_set(d, "content", c);
            }

            continue;
        }
        else
        if (mode == M_TRANSITION) {
            ln = xs_strip_i(ln); /** TRANSITION **/
            xs *c = xs_dup(xs_dict_get(d, "content"));
            c = xs_list_append(c, ln);
            d = xs_dict_set(d, "content", c);

            /* truncate the line */
            ln[0] = '\0';

            mode = M_ACTION;
            goto switch_mode;
        }
        else
        if (mode == M_SCENE) {
            ln = xs_strip_i(ln); /** SCENE **/
            xs *c = xs_dup(xs_dict_get(d, "content"));
            c = xs_list_append(c, ln);
            d = xs_dict_set(d, "content", c);

            /* add to the list of scenes, if it's not already there */
            if (xs_list_in(scenes, ln) == -1)
                scenes = xs_list_append(scenes, ln);

            /* truncate the line */
            ln[0] = '\0';

            mode = M_ACTION;
            goto switch_mode;
        }
        else
        if (mode == M_DIALOG) {
            ln = xs_strip_i(ln); /** DIALOG **/
            xs *c = NULL;

            /* no character set? it's the first line */
            if (xs_dict_get(d, "character") == NULL) {
                d = xs_dict_append(d, "character", ln);

                c = xs_list_new();
                d = xs_dict_set(d, "content", c);

                /* add to the list of characters, if it's not already there */
                char *w = strchr(ln, '(');
                if (w)
                    *w = '\0';
                ln = xs_strip_chars_i(ln, " ^");
                if (xs_list_in(characters, ln) == -1)
                    characters = xs_list_append(characters, ln);
            }
            else
            if (*ln == '\0') {
                /* empty line: end of mode */
                mode = M_ACTION;
                goto switch_mode;
            }
            else {
                /* append to the content */
                c = xs_dup(xs_dict_get(d, "content"));

                c = xs_list_append(c, ln);
                d = xs_dict_set(d, "content", c);
            }

            continue;
        }
        else
        if (mode == M_CENTER) {
            ln = xs_strip_chars_i(ln, "<> "); /** CENTER **/

            xs *c = xs_dup(xs_dict_get(d, "content"));
            c = xs_list_append(c, ln);
            d = xs_dict_set(d, "content", c);

            /* truncate the line */
            ln[0] = '\0';

            mode = M_ACTION;
            goto switch_mode;
        }

switch_mode:
        /* add the unit only if it has content */
        if (xs_list_len(xs_dict_get(d, "content")))
            r = xs_list_append(r, d);

        xs_free(d);
        d = new_unit(m_types[mode]);

        goto again;
    }

    /* add the last unit */
    r = xs_list_append(r, d);
    xs_free(d);

    /* prepend the special objects */
    xs *sl = new_unit("scene_list");
    sl = xs_dict_set(sl, "content", scenes);
    r = xs_list_insert(r, 0, sl);
    xs *cl = new_unit("character_list");
    cl = xs_dict_set(cl, "content", characters);
    r = xs_list_insert(r, 0, cl);

    return r;
}


xs_list *fountain_line(const char *line)
/* splits a line parsing style (bold, etc.) codes */
{
    xs_list *r = xs_list_new();

    xs *l = xs_regex_split(line, "(_[^_]+_|\\*[^*]+\\*|\\*\\*?[^*]+\\*?\\*)");
    const xs_str *v;
    int n = 0;

    xs_list_foreach(l, v) {
        if (n & 0x01) {
            int u = 0;
            int b = 0;
            int i = 0;

            /* iterate the marked text until there are no more marks */
            xs *t = xs_dup(v);

            for (;;) {
                if (xs_match(t, "_*_")) {
                    u++;
                    t = xs_strip_chars_i(t, "_");
                }
                else
                if (xs_match(t, "\\*\\**\\*\\*")) {
                    b++;
                    t = xs_strip_chars_i(t, "*");
                }
                else
                if (xs_match(t, "\\**\\*")) {
                    i++;
                    t = xs_strip_chars_i(t, "*");
                }
                else
                    break;
            }

            if (u) r = xs_list_append(r, "[U]");
            if (i) r = xs_list_append(r, "[I]");
            if (b) r = xs_list_append(r, "[B]");
            r = xs_list_append(r, t);
            if (b) r = xs_list_append(r, "[b]");
            if (i) r = xs_list_append(r, "[i]");
            if (u) r = xs_list_append(r, "[u]");
        }
        else {
            /* store as is */
            r = xs_list_append(r, v);
        }

        n++;
    }

    return r;
}
