/* ventura - a .fountain processor */
/* copyright (c) 2023 - 2024 grunfink - MIT license */

#include "xs.h"
#include "xs_io.h"
#include "xs_json.h"

#include "ventura.h"

int usage(void)
{
    printf("ventura " VERSION " - A .fountain (screenplay markup) processor\n");
    printf("Copyright (c) 2023 - 2024 grunfink - MIT license\n");
    printf("\n");
    printf("Usage:\n");
    printf("\n");
    printf("   ventura [-f {output format}] {input file} [{output file}]\n");
    printf("\n");
    printf("Valid output formats: rtf json\n");
    printf("\n");
    printf("File names can be - (hyphen) to mean stdin or stdout\n");

    return 1;
}


void json_write(const xs_list *play, FILE *f)
{
    xs_json_dump(play, 4, f);
}


int main(int argc, char *argv[])
{
    const char *ifn = NULL;
    const char *ofn = NULL;
    const char *fmt = NULL;
    void (*func)(const xs_list *, FILE *) = NULL;
    int n;

    for (n = 1; n < argc; n++) {
        const char *p = argv[n];

        if (strcmp(p, "-f") == 0) {
            /* force output format */
            fmt = argv[++n];
        }
        else
        if (ifn == NULL)
            ifn = argv[n];
        else
        if (ofn == NULL)
            ofn = argv[n];
    }

    if (ifn == NULL)
        return usage();
    if (ofn == NULL)
        ofn = "-";

    /* if no fmt is set, try to infer from ofn's extension */
    if (fmt == NULL) {
        if ((fmt = strchr(ofn, '.')))
            fmt++;
        else {
            fprintf(stderr, "ERROR: no output format set and cannot infer from '%s'\n", ofn);
            return 2;
        }
    }

    /* set the conversion function */
    if (strcmp(fmt, "rtf") == 0 || strcmp(fmt, "RTF") == 0)
        func = rtf_write;
    else
    if (strcmp(fmt, "json") == 0)
        func = json_write;

    if (func == NULL) {
        fprintf(stderr, "ERROR: unsupported format '%s'\n", fmt);
        return 2;
    }

    FILE *f;
    if (strcmp(ifn, "-") == 0)
        f = fdopen(0, "r");
    else
        f = fopen(ifn, "r");

    if (f == NULL) {
        fprintf(stderr, "ERROR: cannot open '%s' for reading\n", ifn);
        return 2;
    }

    xs *play = fountain_parse(f);
    fclose(f);

    if (strcmp(ofn, "-") == 0) {
        /* if no format is set for stdout, fail */
        if (fmt == NULL)
            return usage();
        else
            f = fdopen(1, "w");
    }
    else
        f = fopen(ofn, "w");

    if (f == NULL) {
        fprintf(stderr, "ERROR: cannot open '%s' for writing\n", ofn);
        return 2;
    }

    func(play, f);
    fclose(f);

    return 0;
}
