PREFIX=/usr/local
PREFIX_MAN=$(PREFIX)/man
CFLAGS?=-g -Wall
NAME=ventura

all: $(NAME)

$(NAME): ventura.o fountain.o rtf.o main.o *.h
	$(CC) $(CFLAGS) *.o $(LDFLAGS) -o $@

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

clean:
	rm -rf *.o *.core $(NAME) makefile.depend

dep:
	$(CC) -MM *.c > makefile.depend

install:
	mkdir -p -m 755 $(PREFIX)/bin
	install -m 755 $(NAME) $(PREFIX)/bin/$(NAME)
	mkdir -p -m 755 $(PREFIX_MAN)/man1
#	install -m 644 doc/$(NAME).1 $(PREFIX_MAN)/man1/$(NAME).1
	mkdir -p -m 755 $(PREFIX_MAN)/man5
#	install -m 644 doc/$(NAME).5 $(PREFIX_MAN)/man5/$(NAME).5

fountain.o: fountain.c xs.h xs_io.h xs_unicode.h xs_json.h xs_match.h \
 xs_regex.h ventura.h
main.o: main.c xs.h xs_io.h xs_json.h ventura.h
rtf.o: rtf.c xs.h xs_io.h xs_unicode.h xs_match.h ventura.h
ventura.o: ventura.c xs.h xs_io.h xs_unicode_tbl.h xs_unicode.h xs_json.h \
 xs_match.h xs_regex.h ventura.h
