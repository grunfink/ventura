/* ventura - a .fountain processor */
/* copyright (c) 2023 - 2024 grunfink - MIT license */

#define VERSION "1.00"

xs_list *fountain_parse(FILE *f);
xs_list *fountain_line(const char *line);
void rtf_write(const xs_list *play, FILE *f);
