/* ventura - a .fountain processor */
/* copyright (c) 2023 - 2024 grunfink - MIT license */

#include "xs.h"
#include "xs_io.h"
#include "xs_unicode.h"
#include "xs_match.h"

#include "ventura.h"

/** templates **/

const char *t_file_s =
    "{\\rtf1\\ansi\\plain\n"
    "{\\*\\generator ventura " VERSION "}\n"
    "\\paperw11906\\paperh16838\n"
    "{\\fonttbl{\\f0\\fmodern\\fprq2\\fcharset0 Courier New;}}\n"
    "{\\footer \\qc{\\field{\\*\\fldinst  PAGE }"
    "{\\fldrslt 1}}}\n"
    ;
const char *t_file_e = "}\n";

const char *t_scene_s = "\\par\\sa0\\li0\\rin0\\i0\\ql\\fi0\\f0\\fs24\n{\\b ";
const char *t_scene_e = "}\n\\par\n";

const char *t_trans_s = "\\par\\sa0\\li0\\rin0\\i0\\qr\\fi0\\f0\\fs24\n";
const char *t_trans_e = "\n\\par\n";

const char *t_action_s = "\\par\\sa0\\li0\\rin0\\i0\\ql\\fi0\\f0\\fs24\n";
const char *t_action_e = "\n\\par\n";

const char *t_center_s = "\\par\\sa0\\li0\\rin0\\i0\\qc\\fi0\\f0\\fs24\n";
const char *t_center_e = "\n\\par\n";

const char *t_character_s = "\\par\\sa0\\li2880\\rin0\\i0\\ql\\fi0\\f0\\fs24\n";
const char *t_character_e = "\n\\par\n";

const char *t_paren_s = "\\sa0\\li2304\\rin0\\i0\\ql\\fi0\\f0\\fs24\n";
const char *t_paren_e = "\n\\par\n";

const char *t_dialog_s = "\\sa0\\li1440\\rin1800\\i0\\ql\\fi0\\f0\\fs24\n";
const char *t_dialog_e = "\n\\par\n";

const char *t_meta_s = "\\par\\sa0\\li0\\rin0\\i0\\qc\\fi0\\f0\\fs24\n{\\b ";
const char *t_meta_e = "}\\par\\par";
const char *t_meta_f = "\\page";


xs_str *rtf_string(const char *str)
/* escape non-ASCII chars */
{
    xs *s = xs_str_new(NULL);
    const char *p = str;
    unsigned int cpoint;

    while ((cpoint = xs_utf8_dec(&p))) {
        if (cpoint <= 128)
            s = xs_utf8_cat(s, cpoint);
        else {
            xs *s1 = xs_fmt("\\u%d?", cpoint);
            s = xs_str_cat(s, s1);
        }
    }

    xs *l = fountain_line(s);
    xs_str *s2 = xs_str_new(NULL);
    const xs_str *v;

    xs_list_foreach(l, v) {
        if (xs_match(v, "[?]")) {
            switch(v[1]) {
            case 'B': v = "\\b1 "; break;
            case 'b': v = "\\b0 "; break;
            case 'I': v = "\\i1 "; break;
            case 'i': v = "\\i0 "; break;
            case 'U': v = "\\ul "; break;
            case 'u': v = "\\ul0 "; break;
            }
        }

        s2 = xs_str_cat(s2, v);
    }

    return s2;
}


void rtf_write(const xs_list *play, FILE *f)
/* writes a play in RTF format */
{
    const xs_dict *u;

    fprintf(f, "%s", t_file_s);

    xs_list_foreach(play, u) {
        const char *type = xs_dict_get(u, "type");
        const xs_list *c = xs_dict_get(u, "content");
        const char *v;

        if (strcmp(type, "header") == 0) {
            xs_list_foreach(c, v) {
                xs *s = rtf_string(v);

                if (*s == '#')
                    s = xs_str_wrap_i("", xs_strip_chars_i(s, "#"), ":");

                fprintf(f, "%s%s%s", t_meta_s, s, t_meta_e);
            }
            fprintf(f, "%s", t_meta_f);
        }
        else
        if (strcmp(type, "scene") == 0) {
            xs *s = rtf_string(xs_list_get(c, 0));
            fprintf(f, "%s%s%s", t_scene_s, s, t_scene_e);
        }
        else
        if (strcmp(type, "action") == 0) {
            xs_list_foreach(c, v) {
                if (*v) {
                    xs *s = rtf_string(v);
                    fprintf(f, "%s%s%s", t_action_s, s, t_action_e);
                }
            }
        }
        else
        if (strcmp(type, "dialog") == 0) {
            xs *s = rtf_string(xs_dict_get(u, "character"));
            fprintf(f, "%s%s%s", t_character_s, s, t_character_e);

            xs_list_foreach(c, v) {
                xs *s1 = rtf_string(v);
                if (v[0] == '(')
                    fprintf(f, "%s%s%s", t_paren_s, s1, t_paren_e);
                else
                    fprintf(f, "%s%s%s", t_dialog_s, s1, t_dialog_e);
            }
        }
        else
        if (strcmp(type, "transition") == 0) {
            xs *s = rtf_string(xs_list_get(c, 0));
            fprintf(f, "%s%s%s", t_trans_s, s, t_trans_e);
        }
        else
        if (strcmp(type, "center") == 0) {
            xs *s = rtf_string(xs_list_get(c, 0));
            fprintf(f, "%s%s%s", t_center_s, s, t_center_e);
        }
    }

    fprintf(f, "%s", t_file_e);
}
