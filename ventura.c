/* ventura - a .fountain processor */
/* copyright (c) 2023 - 2024 grunfink - MIT license */

#define XS_IMPLEMENTATION

#include "xs.h"
#include "xs_io.h"
#include "xs_unicode_tbl.h"
#include "xs_unicode.h"
#include "xs_json.h"
#include "xs_match.h"
#include "xs_regex.h"

#include "ventura.h"
